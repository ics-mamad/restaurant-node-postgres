const yup = require("yup");

const foodNameDescRegex = /^[^\d]+$/; 

const foodValidation = yup.object({
  food_name: yup.string().required().min(3).max(15).matches(foodNameDescRegex, 'Food name must not contain numeric values'),
  food_price: yup.number().required(),
  food_desc: yup.string().required().min(15).matches(foodNameDescRegex, 'Food description must not contain numeric values'),
  rest_id: yup.number().required(),
});

const foodUpdateValidation = yup.object({
  food_name: yup.string().min(3).max(15).matches(foodNameDescRegex, 'Food name must not contain numeric values'),
  food_price: yup.number(),
  food_desc: yup.string().min(15).matches(foodNameDescRegex, 'Food description must not contain numeric values'),
  rest_id: yup.number(),
});

exports.foodValidation = { foodValidation, foodUpdateValidation };
