const yup = require("yup");

const nameCityRegex = /^[^\d]+$/; 

const restValidation = yup.object({
  rest_name: yup.string().min(5).max(15).required().matches(nameCityRegex, 'Restaurant name must not contain numeric values'),
  rest_address: yup.string().min(10).required(),
  rest_city: yup.string().required().matches(nameCityRegex, 'City name must not contain numeric values'),
});

const restUpdateValidation = yup.object({
  rest_name: yup.string().min(5).max(15).matches(nameCityRegex, 'Restaurant name must not contain numeric values'),
  rest_address: yup.string().min(10),
  rest_city: yup.string().matches(nameCityRegex, 'City name must not contain numeric values'),
});

module.exports = {
  restValidation,
  restUpdateValidation,
};
