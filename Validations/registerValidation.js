const yup = require("yup");

const nameGenderRegex = /^[^\d]+$/; // Regular expression to disallow numeric values

const registerValidation = yup.object({
  name: yup.string().required().matches(nameGenderRegex, 'Name must not contain numeric values'),
  age: yup.number().min(12).required(),
  gender: yup.string().required().matches(nameGenderRegex, 'Gender must not contain numeric values'),
  email: yup.string().email().required(),
  password: yup.string().min(6).required(),
});

module.exports = registerValidation;
