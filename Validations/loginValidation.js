const yup = require("yup");

const loginValidation = yup.object({
  email: yup.string().email().required(),
  password: yup.string().min(6).required(),
});

module.exports = loginValidation;
