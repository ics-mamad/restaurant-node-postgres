const pool = require("../db");

const PostFoodDetails = async (req, res) => {
  const { rest_id, food_name, food_price, food_desc } = req.body;

  try {
    const setFoodData = await pool.query(
      "insert into food (rest_id,food_name,food_price,food_desc) values ($1,$2,$3,$4)",
      [rest_id, food_name, food_price, food_desc]
    );
    res.status(200).json(setFoodData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

const GetFoodDetails = async (req, res) => {
  try {
    const getFoodData = await pool.query("select * from food");
    res.status(200).json(getFoodData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};
const PutFoodDetails = async (req, res) => {
  const { food_name, food_price, food_desc } = req.body;
  const { id } = req.params;

  try {
    const updateResData = await pool.query(
      "update food set food_name = ($1), food_price = ($2), food_desc= ($3) where food_id = ($4)",
      [food_name, food_price, food_desc, id]
    );
    res.status(200).json(updateResData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

const DeleteFoodDetails = async (req, res) => {
  const { id } = req.params;

  try {
    const deleteResData = await pool.query(
      "delete from food where food_id = ($1)",
      [id]
    );
    res.status(200).json(deleteResData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

module.exports = {
  PostFoodDetails,
  GetFoodDetails,
  PutFoodDetails,
  DeleteFoodDetails,
};
