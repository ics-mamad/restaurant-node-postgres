const pool = require("../db");

const PostDetails = async (req, res) => {
  const { rest_name, rest_address, rest_city } = req.body;
  //   console.log(req.body)

  try {
    const setResData = await pool.query(
      "insert into restaurantschema (rest_name,rest_address,rest_city) values ($1,$2,$3)",
      [rest_name, rest_city, rest_address]
    );
    res.status(200).json(setResData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

const GetDetails = async (req, res) => {
  try {
    const getResData = await pool.query("select * from restaurantschema");
    res.status(200).json(getResData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

const PutDetails = async (req, res) => {
  const { rest_name, rest_address, rest_city } = req.body;
  const { id } = req.params;

  try {
    console.log("Hello")
    const checkExistingRecord = await pool.query(
      "SELECT * FROM restaurantschema WHERE rest_id = $1",
      [id]
    );

    if (checkExistingRecord.rows.length === 0) {
      return res
        .status(404)
        .json({ message: "Record not found for the provided ID." });
    }

    await pool.query(
      "UPDATE restaurantschema SET rest_name = $1, rest_city = $2, rest_address = $3 WHERE rest_id = $4",
      [rest_name, rest_city, rest_address, id]
    );

    res.status(200).json({ message: "Updated Successfully..." });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

const DeleteDetails = async (req, res) => {
  const { id } = req.params;

  try {
    const deleteResData = await pool.query(
      "delete from restaurantschema where rest_id = ($1)",
      [id]
    );
    res.status(200).json(deleteResData);
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: error.message });
  }
};

module.exports = { PostDetails, GetDetails, PutDetails, DeleteDetails };
