const pool = require("../db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
SECRET_KEY = "RESTAURENTAPI";

const userRegister = async (req, res) => {
  const { name, age, gender, email } = req.body;
  try {
    // Check for existing user in db
    const existingUser = await pool.query(
      "SELECT * FROM users WHERE email = ($1)",
      [email]
    );
    if (existingUser.rows.length > 0) {
      return res.status(400).json({ message: "User already exists !!!" });
    }

    // Hash password
    const hashedPassword = await bcrypt.hash(req.body.password, 12);

    // Create new user
    const userCreate = await pool.query(
      "INSERT INTO users (name, age, gender, email, password) VALUES ($1, $2, $3, $4, $5)",
      [name, age, gender, email, hashedPassword]
    );

    // Create token
    const token = jwt.sign({ email: email, id: userCreate.id }, SECRET_KEY);

    const { password, ...rest } = userCreate;
    const displayData = { ...rest, token };
    res.status(201).json(displayData);
  } catch (error) {
    console.log(error);
    res.status(404).json({ message: "Something Went Wrong !!!" });
  }
};

const userLogin = async (req, res) => {
  const { email } = req.body;

  try {
    // Check for if user is registered or not
    const existingUser = await pool.query(
      "SELECT * FROM users WHERE email = ($1)",
      [email]
    );
    if (!existingUser.rows.length === 0) {
      return res.status(400).json({ message: "User Not Found !!!" });
    }

    // Match password with the existing user's password in the database
    //   const existingUserPass = await pool.query('select password from users where email = ($1)',[email])
    //   console.log(existingUserPass.rows[0].password) //this giving an error bcz of objecttyp
    const hashedPassword = existingUser.rows[0].password;
    //   console.log(hashedPassword)

    const matchPassword = await bcrypt.compare(
      req.body.password,
      hashedPassword
    );

    if (!matchPassword) {
      return res.status(400).json({ message: "Invalid Credentials !!!" });
    }

    // Create a new token
    const token = jwt.sign(
      { email: existingUser.email, id: existingUser.id },
      SECRET_KEY,
      { expiresIn: "1h" }
    );

    // Store session in the database
    //   await Session.create({ userId: existingUser._id, token ,os_module:os.platform()});

    // req.session.user = {existingUser,is_login:true};

    // console.log(req.session.user);

    const { password, ...rest } = existingUser;
    const displayData = { ...rest, token };

    res.status(201).json(displayData);
  } catch (error) {
    console.log(error);
    res.status(404).json({ message: "Something Went Wrong !!!" });
  }
};

module.exports = {
  userRegister,
  userLogin,
};
