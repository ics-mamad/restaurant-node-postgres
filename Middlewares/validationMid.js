const validationMid = (schema) => (req, res, next) => {
  const body = req.body;
  try {
    schema.validateSync(body);
    next();
  } catch (error) {
    return res.status(400).json({ errmsg: error.message });
  }
};
module.exports = validationMid;
