const jwt = require("jsonwebtoken");
const SECRET_KEY = "RESTAURENTAPI";
// const pool = require('../db')

const auth = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    if (token) {
      token = token.split(" ")[1];
      let user = jwt.verify(token, SECRET_KEY);
      req.userId = user.id;

      // Check if session is active
      // const activeSession = await Session.findOne({ userId: user.id, token });
      // if (!activeSession) {
      //     throw new Error("Unauthorized User !");
      // }

      next();
    } else {
      throw new Error("Unauthorized User !!!");
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({
      message: error.message,
    });
  }
};

module.exports = auth;
