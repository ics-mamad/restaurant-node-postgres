const {
  PostFoodDetails,
  GetFoodDetails,
  PutFoodDetails,
  DeleteFoodDetails,
} = require("../Controllers/FoodController");

const express = require("express");
const auth = require("../Middlewares/auth");
const foodRouter = express.Router();

foodRouter.route("/food").post(auth, PostFoodDetails).get(auth, GetFoodDetails);

foodRouter
  .route("/food/:id")
  .put(auth, PutFoodDetails)
  .delete(auth, DeleteFoodDetails);

module.exports = foodRouter;
