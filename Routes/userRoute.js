const { userRegister, userLogin } = require("../Controllers/userController");
const express = require("express");
const userRouter = express.Router();
const validationMid = require("../Middlewares/validationMid");
const loginValidation = require("../Validations/loginValidation");
const registerValidation = require("../Validations/registerValidation");

userRouter.post("/register", validationMid(registerValidation), userRegister);
userRouter.post("/login", validationMid(loginValidation), userLogin);

module.exports = userRouter;
