const {
  PostDetails,
  GetDetails,
  PutDetails,
  DeleteDetails,
} = require("../Controllers/restaurantController");
const express = require("express");
const restRouter = express.Router();
const auth = require("../Middlewares/auth");

restRouter.route("/restaurant").post(auth, PostDetails).get(auth, GetDetails);

restRouter
  .route("/restaurant/:id")
  .put(auth, PutDetails)
  .delete(auth, DeleteDetails);

module.exports = restRouter;
