const express = require("express");
const app = express();
const port = 3000;
const pool = require("./db");
const userRouter = require("./Routes/userRoute");
const restRoute = require("./Routes/restaurantRoute");
const foodRouter = require("./Routes/foodRoute");
app.use(express.json());

app.use("/auth", userRouter);
app.use("/rest", restRoute);
app.use("/fd", foodRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});
